#include "mydirview.hpp"
#include <QScreen>
#include <QDebug>
#include <QScroller>

wrk::Dirview::Dirview(QWidget * parent):
    QWidget(parent),
    tree(QTreeView()),
    lineEdit(QLineEdit()),
    rootPath(getRootPath()),
    model(QFileSystemModel()),
    layout(QVBoxLayout())
{
    model.setRootPath("");
    model.setFilter(QDir::AllEntries | QDir::Hidden);
    model.setOption(QFileSystemModel::DontUseCustomDirectoryIcons);
    model.setOption(QFileSystemModel::DontWatchForChanges);

    tree.setModel(&model);
    const QModelIndex rootIndex = model.index(QDir::cleanPath(rootPath));
    tree.setRootIndex(rootIndex);

    tree.setAnimated(false);
    tree.setIndentation(20);
    tree.setSortingEnabled(true);
    tree.setColumnWidth(0, tree.width() / 3);

    QScroller::grabGesture(&tree, QScroller::TouchGesture);

    layout.setSpacing(1);
    layout.addWidget(&lineEdit);
    layout.addWidget(&tree);

    setLayout(&layout);

    connect(&lineEdit, &QLineEdit::textChanged, this, &Dirview::sortByInput);
}

QString wrk::Dirview::getRootPath() const
{
    QString path = "/home/";
    path.append(qgetenv("USER").constData());
    return path;
}

void wrk::Dirview::sortByInput()
{
    names.clear();
    QString input = lineEdit.text();
    if (input.isEmpty())
    {
        model.setFilter(QDir::AllEntries | QDir::Hidden);
    }
    else
    {
        names.append(input);
    }
    model.setNameFilters(names);
    tree.repaint();
}
