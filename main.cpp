#include <QApplication>
#include <QScreen>
#include "mydirview.hpp"

int main(int argc, char ** argv)
{
    QApplication app(argc, argv);
    wrk::Dirview window;
    window.resize(window.screen()->availableGeometry().size() / 2);
    window.show();
    return app.exec();
}
