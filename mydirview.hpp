#ifndef MY_DIR_VIEW
#define MY_DIR_VIEW

#include <QWidget>
#include <QTreeView>
#include <QFileSystemModel>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QString>
#include <QStringList>

namespace wrk
{
  class Dirview : public QWidget
  {
  Q_OBJECT

  public:
    Dirview(QWidget * parent = 0);

  public slots:
    void sortByInput();
 
  private:
    QTreeView tree;
    QLineEdit lineEdit;
    QString rootPath;
    QFileSystemModel model;
    QVBoxLayout layout;
    QStringList names;

    QString getRootPath() const;
  };
}

#endif
